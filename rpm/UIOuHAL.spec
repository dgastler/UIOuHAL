#
# spefile for UIOuHAL CLI
#
Name: %{name} 
Version: %{version} 
Release: %{release} 
Packager: %{packager}
Summary: UIOuHAL
License: Apache License
Group: BUTools
Source: https://gitlab.com/BU-EDF/sw/UIOuHAL
URL: https://gitlab.com/BU-EDF/sw/UIOuHAL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
UIOuHAL plugin for uHAL

%prep

%build

%install 

# copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}
cp -rp %{sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.

#Change access rights
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/lib

%clean 

%post 

%postun 

%files 
%defattr(-, root, root) 
%{_prefix}/lib/*
%{_prefix}/include/*


